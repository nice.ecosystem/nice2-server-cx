package net.ninmedia.nice2.cx.controller;

import net.ninmedia.nice2.core.ex.BasicError;
import net.ninmedia.nice2.core.ex.StbNotFound;
import net.ninmedia.nice2.core.util.ApiAuth;
import net.ninmedia.nice2.core.util.CxUtil;
import net.ninmedia.nice2.core.util.SqlParams;
import net.ninmedia.nice2.core.util.TextUtil;
import net.ninmedia.nice2.cx.activity.StbActivity;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.UUID;

@RestController
public class RestApi {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    CxUtil cxUtil;
    @Autowired
    TextUtil textUtil;
    @Autowired
    StbActivity stbActivity;
    @Autowired
    ApiAuth apiAuth;
    @Autowired
    HttpServletRequest request;

    private JSONObject user;
    private JSONObject stb;
    private JSONObject req;
    private JSONObject activity;

    @RequestMapping(value = {"/req"}, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public String post() {
        JSONObject respJson = new JSONObject();
        String refId = UUID.randomUUID().toString();
        respJson.put("success", false);
        respJson.put("refId", refId);

        try {
            String reqBody = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);
            req = new JSONObject(reqBody);
            user = apiAuth.authUser();
            stb = cxUtil.getStbDetail(req.getString("stbId"));

            activity = new JSONObject();
            activity.put("refId", refId);
            activity.put("reqId", req.get("refId"));

            long reqId = addReqLog();

            activity.put("reqId", reqId);
            activity.put("req", req);
            activity.put("user", user);
            activity.put("stb", stb);

            String methodName = req.getString("method");
            Method method = stbActivity.getClass().getMethod(methodName, JSONObject.class);
            method.invoke(stbActivity, activity);

            respJson.put("success", true);

        } catch (JSONException e) {
            respJson.put("error","Error JSON format!");
        } catch (BasicError e) {
            int errorId = e.getId();
            respJson.put("errorId",errorId);
            respJson.put("errorMsg",e.getMessage());
            respJson.put("errorDescription",textUtil.getText(errorId));
        } catch (InvocationTargetException e) {
            Throwable t = e.getCause();
            log.error("Error on calling method: "+t,t);
            respJson.put("errorMsg", ""+t.getMessage());
        } catch (Exception e) {
            respJson.put("errorMsg", ""+e.getMessage());
        }

        return respJson.toString();
    }

    private long addReqLog() {
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("userId", user.getLong("id"));
        params.addValue("reqId", req.get("refId"));
        params.addValue("refId", activity.get("refId"));
        params.addValue("method", req.get("method"));
        params.addValue("stbId", stb.get("id"));

        JSONObject jsonData = (req.has("data"))?req.getJSONObject("data"):new JSONObject();

        params.addValue("qr", stb.get("qr"));
        params.addValue("chipId", stb.get("chip_id"));

        params.addValue("param1", (jsonData.has("param1"))?jsonData.get("param1"):null);
        params.addValue("param2", (jsonData.has("param2"))?jsonData.get("param2"):null);
        params.addValue("param3", (jsonData.has("instId"))?jsonData.get("param3"):null);
        params.addValue("param4", (jsonData.has("instId"))?jsonData.get("param4"):null);
        params.addValue("param4", (jsonData.has("instId"))?jsonData.get("param4"):null);

        params.addValue("reqFrom", request.getRemoteAddr());

        KeyHolder holder = new GeneratedKeyHolder();

        String sql = " INSERT INTO cx_req_log (user_id,req_id,ref_id,method,data_qr,data_chip_id,data_param1,data_param2,data_param3,data_param4,req_from,stb_id) VALUES " +
                " (:userId,:reqId,:refId,:method,:qr,:chipId,:param1,:param2,:param3,:param4,:reqFrom,:stbId) ";

        jdt.update(sql, params, holder);
        return holder.getKey().longValue();
    }
}
