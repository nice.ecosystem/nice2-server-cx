package net.ninmedia.nice2.cx.activity;

import net.ninmedia.nice2.core.ex.CxException;
import net.ninmedia.nice2.core.util.MqUtil;
import net.ninmedia.nice2.core.util.TextUtil;
import net.ninmedia.nice2.cx.util.ByteBuilder;
import net.ninmedia.nice2.cx.util.CxActivity;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeParser;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

import static net.ninmedia.nice2.cx.service.CasSumaConnection.QUEUE_CAS_SUMA;

@Component
public class ProductActivation extends StbBaseActivity {
    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    MqUtil mqUtil;
    @Autowired
    TextUtil textUtil;
    @Autowired
    CxActivity cxActivity;
    @Autowired
    StbActivation stbActivation;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final static int COMMAND_TYPE_ID = 1;

    public void activate(JSONObject reqJson, boolean deactivated) throws Exception {
        stbActivation.activate(reqJson);

        JSONObject jsonData = reqJson.getJSONObject("req").getJSONObject("data");
        JSONObject jsonStb = reqJson.getJSONObject("stb");
        String productKeyword = jsonData.getString("param1");
        JSONObject jsonProduct = getProduct(productKeyword);

        DateTime beginDateTime;
        DateTime endDateTime;

        if (deactivated) {
            beginDateTime = DateTime.now().plusDays(3);
            endDateTime = DateTime.now().plusDays(1);
        } else {
            beginDateTime = DateTime.parse(jsonData.getString("param2"), dtf);
            endDateTime = DateTime.parse(jsonData.getString("param3"), dtf);
        }

        final byte commandId = COMMAND_TYPE_ID;
        final short productId = (short) jsonProduct.getInt("product_code");
        final int cardId = jsonStb.getInt("chip_id");
        final byte productAmount = 1;
        final byte sendOrNot = 1;
        final byte tapingCtrl = 0;
        final int beginDateTimeS = Math.toIntExact(beginDateTime.getMillis()/1000);
        final int endDateTimeS = Math.toIntExact(endDateTime.getMillis()/1000);
        final byte descLen = 0;
        final short sessionId = cxActivity.getSessionId(reqJson);

        ByteBuilder bodyByteBulder = ByteBuilder.create();
        bodyByteBulder.put(cardId);
        bodyByteBulder.put(productAmount);
        bodyByteBulder.put(sendOrNot);
        bodyByteBulder.put(tapingCtrl);
        bodyByteBulder.put(productId);
        bodyByteBulder.put(beginDateTimeS);
        bodyByteBulder.put(endDateTimeS);
        bodyByteBulder.put(descLen);

        log.info("["+cardId+"] Product ID: "+productId);
        log.info("["+cardId+"] Is deactivation?: "+deactivated);
        log.info("["+cardId+"] Start time: "+(new DateTime(beginDateTimeS*1000L)).toString(dateFormat));
        log.info("["+cardId+"] End time: "+(new DateTime(endDateTimeS*1000L)).toString(dateFormat));

        reqJson.put("packet", cxActivity.createJsonPacket(commandId, sessionId, bodyByteBulder.getBytes()));

        String msgId = mqUtil.sendJson(QUEUE_CAS_SUMA, reqJson, 3000, true);
        JSONObject respJson = mqUtil.createJsonReceiver(msgId, 60);
        if (!respJson.getBoolean("success")) {
            throw new Exception("Error send OSD: "+respJson.getString("error"));
        }

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("stbId", jsonStb.get("id"));
        params.addValue("productId", jsonProduct.get("id"));
        params.addValue("refId", reqJson.get("refId"));
        params.addValue("startOn", beginDateTime.toString(dateFormat));
        params.addValue("endOn", endDateTime.toString(dateFormat));

        String sql;
        if (!deactivated) {
            sql = " INSERT INTO cx_stb_bound_product (stb_id,product_id,ref_id,start_on,end_on) VALUES (:stbId,:productId,:refId,:startOn,:endOn) " +
                    " ON DUPLICATE KEY UPDATE start_on=:startOn, end_on=:endOn, ref_id=:refId, enabled=1 ";
        } else {
            sql = " UPDATE cx_stb_bound_product SET enabled=0 WHERE stb_id=:stbId AND product_id=:productId ";
        }
        jdt.update(sql, params);
    }

    public void activate(JSONObject reqJson) throws Exception {
        activate(reqJson, false);
    }


    private JSONObject getProduct(String keyword) throws Exception {
        try {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("productAlias", keyword);

            String sql = " SELECT * FROM cx_cas_product WHERE product_alias=:productAlias ";
            List<Map<String,Object>> rows = jdt.queryForList(sql, params);
            if (rows.size()<=0) throw new CxException(CxException.CAS_INVALID_PRODUCT, "Product parameter is invalid!");
            return new JSONObject(rows.get(0));
        } catch (Exception e) {
            if (e instanceof CxException) throw ((CxException) e);
            throw new CxException(CxException.GENERAL_ERROR, ""+e.getMessage());
        }
    }
}
