package net.ninmedia.nice2.cx.activity;

import net.ninmedia.nice2.core.util.CxUtil;
import net.ninmedia.nice2.core.util.MqUtil;
import net.ninmedia.nice2.core.util.TextUtil;
import net.ninmedia.nice2.cx.util.ByteBuilder;
import net.ninmedia.nice2.cx.util.CxActivity;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.UUID;

import static net.ninmedia.nice2.cx.service.CasSumaConnection.QUEUE_CAS_SUMA;

@Component
public class StbActivation extends StbBaseActivity {
    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    MqUtil mqUtil;
    @Autowired
    TextUtil textUtil;
    @Autowired
    CxActivity cxActivity;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final static int COMMAND_TYPE_ID = 33;

    public void activate(JSONObject reqJson) throws Exception {
        byte commandId = Byte.parseByte(""+COMMAND_TYPE_ID);
        JSONObject jsonData = reqJson.getJSONObject("req").getJSONObject("data");
        JSONObject jsonStb = reqJson.getJSONObject("stb");

        int cardId = jsonStb.getInt("chip_id");
        log.info("Card ID#"+cardId);
        ByteBuilder bodyByteBulder = ByteBuilder.create();
        bodyByteBulder.put(cardId);
        bodyByteBulder.put(cxActivity.getDefaultExpTime());
        bodyByteBulder.put((byte) 1);
        byte[] body = bodyByteBulder.getBytes();

        final short sessionId = cxActivity.getSessionId(reqJson);
        log.info("Session ID: "+sessionId);

        JSONObject jsonPacket = cxActivity.createJsonPacket(commandId, sessionId, body);

        reqJson.put("packet", jsonPacket);

        String msgId = mqUtil.sendJson(QUEUE_CAS_SUMA, reqJson, 0, true);
        JSONObject respJson = mqUtil.createJsonReceiver(msgId, 60);
        if (!respJson.getBoolean("success")) {
            throw new Exception("Error activating STB: "+respJson.getString("error"));
        }
    }
}
