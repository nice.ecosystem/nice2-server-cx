package net.ninmedia.nice2.cx.activity;

import net.ninmedia.nice2.core.util.CxUtil;
import net.ninmedia.nice2.core.util.TextUtil;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

@Component
public class StbActivity {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    CxUtil cxUtil;
    @Autowired
    TextUtil textUtil;
    @Autowired
    StbActivation stbActivation;
    @Autowired
    OsdMessage osdMessage;
    @Autowired
    ProductActivation productActivation;

    public void activation(JSONObject reqJson) throws Exception {
        JSONObject stb = reqJson.getJSONObject("stb");
        log.info("Activating STB: "+stb);
        stbActivation.activate(reqJson);
    }

    public void osd(JSONObject reqJson) throws Exception {
        JSONObject stb = reqJson.getJSONObject("stb");
        log.info("Activating STB: "+stb);
        osdMessage.send(reqJson);
    }

    public void authorization(JSONObject reqJson) throws Exception {
        JSONObject stb = reqJson.getJSONObject("stb");
        log.info("Activating STB: "+stb);
        productActivation.activate(reqJson);
    }

    public void deauthorization(JSONObject reqJson) throws Exception {
        JSONObject stb = reqJson.getJSONObject("stb");
        log.info("Activating STB: "+stb);
        productActivation.activate(reqJson, true);
    }
}
