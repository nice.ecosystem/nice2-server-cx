package net.ninmedia.nice2.cx.activity;

import net.ninmedia.nice2.core.util.MqUtil;
import net.ninmedia.nice2.core.util.TextUtil;
import net.ninmedia.nice2.cx.util.ByteBuilder;
import net.ninmedia.nice2.cx.util.CxActivity;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

import static net.ninmedia.nice2.cx.service.CasSumaConnection.QUEUE_CAS_SUMA;

@Component
public class OsdMessage {
    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    MqUtil mqUtil;
    @Autowired
    TextUtil textUtil;
    @Autowired
    CxActivity cxActivity;
    @Autowired
    StbActivation stbActivation;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final static int COMMAND_TYPE_ID = 2;

    public void send(JSONObject reqJson) throws Exception {
        stbActivation.activate(reqJson);

        JSONObject jsonData = reqJson.getJSONObject("req").getJSONObject("data");
        JSONObject jsonStb = reqJson.getJSONObject("stb");

        String msg = jsonData.getString("param1");

        final byte commandId = COMMAND_TYPE_ID;
        final short showTimeLen = 0;
        final byte showTimes = Byte.parseByte(cxActivity.getConfig("suma.osd.showtimes"));
        final byte showType = Byte.parseByte(cxActivity.getConfig("suma.osd.showtype"));
        msg = (msg.length()>120)?msg.substring(0, 120).trim():msg.trim();
        final byte msgLength = (byte) msg.length();
        final int expTime = cxActivity.getDefaultExpTime();
        final int cardId = jsonStb.getInt("chip_id");
        log.info("Card ID#"+cardId);
        final short sessionId = (short) cxActivity.getSessionId(reqJson);

        ByteBuilder bodyByteBulder = ByteBuilder.create();
        bodyByteBulder.put(cardId);
        bodyByteBulder.put(showTimeLen);
        bodyByteBulder.put(showTimes);
        bodyByteBulder.put(showType);
        bodyByteBulder.put(expTime);
        bodyByteBulder.put(msgLength);
        bodyByteBulder.put(msg);

        reqJson.put("packet", cxActivity.createJsonPacket(commandId, sessionId, bodyByteBulder.getBytes()));

        String msgId = mqUtil.sendJson(QUEUE_CAS_SUMA, reqJson, 3000, true);
        JSONObject respJson = mqUtil.createJsonReceiver(msgId, 60);
        if (!respJson.getBoolean("success")) {
            throw new Exception("Error send OSD: "+respJson.getString("error"));
        }
    }
}
