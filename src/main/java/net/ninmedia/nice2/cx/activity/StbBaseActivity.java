package net.ninmedia.nice2.cx.activity;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.lang.invoke.MethodHandles;

import java.util.Calendar;

public abstract class StbBaseActivity {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static final String dateFormat = "yyyy-MM-dd HH:mm:ss";
    static final DateTimeFormatter dtf = DateTimeFormat.forPattern(dateFormat);

}
