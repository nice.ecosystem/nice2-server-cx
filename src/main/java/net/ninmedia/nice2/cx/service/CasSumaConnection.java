package net.ninmedia.nice2.cx.service;

import net.ninmedia.nice2.core.ex.CxException;
import net.ninmedia.nice2.core.util.MqUtil;
import net.ninmedia.nice2.cx.util.CxActivity;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.Session;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.invoke.MethodHandles;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Arrays;

@Component
public class CasSumaConnection {
    public static final String QUEUE_CAS_SUMA = "cas-suma-connection";
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    MqUtil mqUtil;
    @Autowired
    CxActivity cxActivity;

    private boolean needReply = false;
    private String replyTo = null;

    @JmsListener(destination = QUEUE_CAS_SUMA)
    public void receiveMessage(@Payload String rawString, @Headers MessageHeaders headers, Message message, Session session) {
        try {
            needReply = message.getBooleanProperty("needReply");
            if (needReply) {
                replyTo = message.getJMSCorrelationID();
            }
            log.info("Receiving message: "+rawString+" - "+replyTo+", need reply: "+needReply);

            sendToCas(new JSONObject(rawString));

            if (needReply) {
                mqUtil.sendText(replyTo,new JSONObject().put("success", true).toString());
            }
        } catch (Exception e) {
            log.error("Error receiving messsage.."+e,e);
            try {
                mqUtil.sendText(replyTo, new JSONObject().put("success", false).put("error",e.getMessage()).toString());
            } catch (Exception e1) {};
        }
    }

    private void sendToCas(JSONObject jsonReq) throws CxException {
        Long logId = 0L;
        try {
            JSONObject stb = jsonReq.getJSONObject("stb");
            int chipId = stb.getInt("chip_id");

            log.info("[" + chipId + "] ==== Ref Req: " + jsonReq.getString("refId") + " ======");
            JSONObject jsonPacket = jsonReq.getJSONObject("packet");

            byte commandId = (byte) jsonPacket.getInt("commandId");
            short sessionId = (short) jsonPacket.getInt("sessionId");

            final byte[] body = Base64.decodeBase64(jsonPacket.getString("body"));
            final byte[] packet = cxActivity.buildPacket(commandId, sessionId, body);
            final byte[] header = Arrays.copyOf(packet, packet.length - body.length);

            logId = cxActivity.addCasLog(jsonReq.getString("refId"), commandId, sessionId, header, body);

            log.info("[" + chipId + "] Req. header: " + cxActivity.getByteString(header));
            log.info("[" + chipId + "] Req. body: " + cxActivity.getByteString(body));
            log.info("[" + chipId + "] Req. packet: " + cxActivity.getByteString(packet));

            String casHost = stb.getString("cas_ip");
            int casPort = stb.getInt("cas_port");

            log.info("[" + chipId + "] Connecting to " + casHost + ":" + casPort + " ...");

            final Socket socket = new Socket();
            socket.setSoTimeout(3000);
            socket.connect(new InetSocketAddress(casHost, casPort), 3000);
            log.info("[" + chipId + "] Socket to " + casHost + ":" + casPort + " is connected!");
            OutputStream output = socket.getOutputStream();
            output.write(packet);

            InputStream input = socket.getInputStream();
            byte[] respHeader = new byte[6];
            input.read(respHeader);
            log.info("[" + chipId + "] Response header data: " + cxActivity.getByteString(respHeader));

            ByteBuffer byteBuffer = ByteBuffer.wrap(respHeader);

            short respSessionId = byteBuffer.getShort();
            byte respCasVersion = byteBuffer.get();
            byte respCommandId = byteBuffer.get();
            short respBodyLength = byteBuffer.getShort();

            log.info("[" + chipId + "] Body length: " + respBodyLength);

            if (respBodyLength <= 0) throw new Exception("Unknown response from CAS, body is 0 byte");

            byte[] respBody = new byte[respBodyLength];
            input.read(respBody);
            log.info("[" + chipId + "] Response body data: " + cxActivity.getByteString(respBody));
            byteBuffer.clear();
            byteBuffer = ByteBuffer.wrap(respBody);

            int respErrorIntCode = byteBuffer.getInt();

            log.info("[" + chipId + "] Response sessionId: " + sessionId + ", casVersion: " + respCasVersion + ", commandId: " + commandId + ", body length: " + respBodyLength + ", error code: " + respErrorIntCode + " (0x" + Integer.toHexString(respErrorIntCode).toUpperCase() + ") ");

            try {
                socket.close();
                log.debug("[" + chipId + "] Socket status closed? " + socket.isClosed());
            } catch (Exception x) {
                log.error("Error closing socket: "+x,x);
            }

            cxActivity.updateCasLog(logId, respHeader, respBody, respErrorIntCode);

            if (respErrorIntCode != 0) {
                throw new CxException(CxException.CAS_INVALID_RESP, "CAS unexpected error code: " + " 0x" + Integer.toHexString(respErrorIntCode).toUpperCase());
            }
        } catch (CxException e) {
            throw e;
        } catch (IOException e) {
            log.error("Error send to CAS: "+e,e);
            cxActivity.updateCasLog(logId, e.getMessage());
            throw new CxException(CxException.CAS_CONNECTION_PROBLEM, e.getMessage());
        } catch (Exception e) {
            log.error("Error send to CAS: "+e,e);
            cxActivity.updateCasLog(logId, e.getMessage());
            throw new CxException(CxException.CAS_GENERAL_ERROR, e.getMessage());
        }
    }
}
