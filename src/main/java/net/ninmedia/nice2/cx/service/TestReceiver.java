package net.ninmedia.nice2.cx.service;

import net.ninmedia.nice2.core.util.MqUtil;
import org.apache.activemq.artemis.jms.client.ActiveMQDestination;
import org.apache.activemq.artemis.jms.client.ActiveMQJMSConnectionFactory;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.*;
import java.lang.invoke.MethodHandles;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

@Component
public class TestReceiver implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    MqUtil mqUtil;

    @JmsListener(destination = "test-queue")
    public void receiveMessage(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        try {
            log.info("Receiving message: "+jsonObjectString);
        } catch (Exception e) {
            log.error("Error receiving messsage.."+e,e);
        }
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        String queueName = UUID.randomUUID().toString();
        //mqUtil.sendText(queueName, "TEST SAJA DENGAN DELAY "+ DateTime.now(), 5000, );
        //mqUtil.sendText("test-queue", "XXX "+ DateTime.now(), 3000);
        //String result = mqUtil.createReceiver(queueName, 30);
        //log.info("Result: "+result);
    }
}
