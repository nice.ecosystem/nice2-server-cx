package net.ninmedia.nice2.cx.util;

import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static net.ninmedia.nice2.cx.service.CasSumaConnection.QUEUE_CAS_SUMA;

@Component
public class CxActivity {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    public static final String dateTimePattern = "yyyy-MM-dd HH:mm:ss";

    @Autowired
    NamedParameterJdbcTemplate jdt;

    public short getSessionId(JSONObject jsonReq) {
        final String reqRef = jsonReq.getString("refId");
        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("reqRef", reqRef);

        String sql = " SELECT IFNULL((MAX(id) % 32766)+1,1) FROM  cx_cas_log WHERE ref_id=:reqRef ";
        return jdt.queryForObject(sql, sqlParams, Short.class);
    }

    public byte[] buildPacket(final byte commandId, short sessionId, final byte[] byteBody) throws IOException {
        short bodyLength = (short) byteBody.length;

        final byte casVersion = Byte.parseByte(getConfig("suma.cas.version"));
        log.info("Command ID: "+commandId+", Session ID: "+sessionId+", cas version: "+casVersion);
        log.info("Data body length: "+bodyLength);

        ByteBuilder packetBuilder = ByteBuilder.create();
        packetBuilder.put(sessionId);
        packetBuilder.put(casVersion);
        packetBuilder.put(commandId);
        packetBuilder.put((short) byteBody.length);
        packetBuilder.put(byteBody);

        return packetBuilder.getBytes();
    }

    public String getConfig(String key) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("key", key);
        String sql = " SELECT cfg_val FROM cx_config WHERE cfg_key=:key ";
        try {
            String val = jdt.queryForObject(sql, params, String.class);
            return val;
        } catch (Exception e) {
            return null;
        }
    }

    public String getByteString(byte[] packet) {
        String result = "";
        for (int i=0;i<packet.length;i++) {
            result+=String.format("%02X ", packet[i]);
        }
        return result.trim();
    }

    public int getDefaultExpTime() {
        Calendar expCal = Calendar.getInstance();
        //int ttlDays = Integer.parseInt(Utils.getConfig("suma.packet.ttl.days"));
        expCal.add(Calendar.DATE, 1);

        int expTime = (int) (expCal.getTimeInMillis()/1000L);

        log.info("Exp. time: "+(new DateTime(Long.valueOf(expTime))).toString("yyyy-MM-dd HH:mm:ss"));
        log.info("Exp. Time (Unix): "+expTime);

        return expTime;
    }

    public JSONObject createJsonPacket(byte commandId, short sessionId, byte[] body) {
        JSONObject jsonPacket = new JSONObject();
        jsonPacket.put("body", Base64.encodeBase64String(body));
        jsonPacket.put("commandId", commandId);
        jsonPacket.put("sessionId", sessionId);
        return jsonPacket;
    }

    public Long addCasLog(String reqRef, byte commandId, short sessionId, byte[] header, byte[] body) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("commandId", commandId);
        params.addValue("reqRef",reqRef);
        params.addValue("sessionId", sessionId);
        params.addValue("reqHeader", getByteString(header).trim());
        params.addValue("reqBody", getByteString(body).trim());

        String sql = " INSERT INTO cx_cas_log (ref_id,command_id,session_id,req_header,req_body) VALUES (:reqRef,:commandId,:sessionId,:reqHeader,:reqBody)  ";
        KeyHolder holder = new GeneratedKeyHolder();
        jdt.update(sql, params, holder);

        return holder.getKey().longValue();
    }

    public void updateCasLog(long logId, byte[] respHeader, byte[] respBody, Integer respStatus, String error) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("logId", logId);
        params.addValue("respHeader", (respHeader!=null)?getByteString(respHeader).trim():null);
        params.addValue("respBody", (respBody!=null)?getByteString(respBody).trim():null);
        params.addValue("error", error);
        params.addValue("respStatus", respStatus);

        String sql = " UPDATE cx_cas_log SET resp_header=:respHeader, resp_body=:respBody, error=:error, resp_status=:respStatus WHERE id=:logId ";
        jdt.update(sql, params);
    }

    public void updateCasLog(long logId, byte[] respHeader, byte[] respBody, int respStatus) {
        updateCasLog(logId, respHeader, respBody, respStatus, null);
    }

    public void updateCasLog(long logId, String error) {
        updateCasLog(logId, null, null, null, error);
    }

    public DateTimeFormatter getDateTimeFormater() {
        return DateTimeFormat.forPattern(dateTimePattern);
    }

    public SimpleDateFormat getSimpleDateFormat() {
        return new SimpleDateFormat(dateTimePattern);
    }

    public void sendToCas() {

    }
}
