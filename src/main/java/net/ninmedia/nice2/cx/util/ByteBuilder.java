package net.ninmedia.nice2.cx.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class ByteBuilder {
	final ByteArrayOutputStream dataBody = new ByteArrayOutputStream();

	public static ByteBuilder create() {
		return new ByteBuilder();
	}

	public ByteBuilder put(short value) throws IOException {
		dataBody.write(ByteBuffer.allocate(2).putShort(value).array());
		return this;
	}

	public ByteBuilder put(int value) throws IOException {
		dataBody.write(ByteBuffer.allocate(4).putInt(value).array());
		return this;
	}

	public ByteBuilder put(String value) throws IOException {
		dataBody.write(ByteBuffer.allocate(value.length()).put(value.getBytes()).array());
		return this;
	}
	
	public ByteBuilder put(byte value) throws IOException {
		dataBody.write(ByteBuffer.allocate(1).put(value).array());
		return this;
	}
	
	public ByteBuilder put(byte[] value) throws IOException {
		dataBody.write(ByteBuffer.allocate(value.length).put(value).array());
		return this;
	}
	
	public byte[] getBytes() {
		return dataBody.toByteArray();
	}
	
	public int size() {
		return dataBody.size();
	}
}
