package net.ninmedia.nice2.cx;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import javax.annotation.PostConstruct;
import java.lang.invoke.MethodHandles;
import java.util.TimeZone;

@SpringBootApplication(scanBasePackages = {"net.ninmedia.nice2"})
public class Application {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static void main(String[] args) {
        ApplicationContext context =  SpringApplication.run(Application.class, args);
    }

    @PostConstruct
    public void init(){
        // Setting Spring Boot SetTimeZone
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Jakarta"));
        log.info("Timezone was set! "+ DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
    }
}